﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SlotAnswer : MonoBehaviour, IHasChanged
{
    [SerializeField] Transform slots;
    [SerializeField] Text answersText;

    // Use this for initialization
    void Start () {
        HasChanged();
    }

    public void HasChanged()
    {
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        foreach (Transform slotTransform in slots)
        {
            GameObject item = slotTransform.GetComponent<Slot>().item;
            if (item)
            {
                builder.Append(item.name);
            }
        }

        if (builder.Length == 5)
        { 
            if (GameScript.answer == builder.ToString())
            {
                answersText.text = "CORRECT!";
                GameScript.Winner = true;
                MenuScript.correctCount++;
            } else
            {
                answersText.text = "WRONG PATTERN!";
            }
        }
    }
}

namespace UnityEngine.EventSystems
{
    public interface IHasChanged : IEventSystemHandler
    {
        void HasChanged();
    }
}
