﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScript : MonoBehaviour
{
    public Canvas PopUpCanvas;

    // Use this for initialization
    void Start () {
        PopUpCanvas.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void QuitGame()
    {
        PopUpCanvas.enabled = true;
    }

    public void QuitConfirmGame()
    {
        Application.Quit();
    }

    public void CancelConfirmGame()
    {
        PopUpCanvas.enabled = false;
    }
}
