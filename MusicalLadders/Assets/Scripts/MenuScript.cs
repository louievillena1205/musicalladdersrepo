﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

    public static float time = 61.0f;
    public static int correctCount = 0;
    // Use this for initialization
    void Start () {
        time = 61.0f;
        correctCount = 0;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
