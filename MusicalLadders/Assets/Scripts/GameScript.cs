﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameScript : MonoBehaviour {

    public static bool GameOver = false;
    public static bool Winner = false;
    
    public Text Timer;
    public static string st = "ABCDEFG";
    public static char clue;
    public static string answer;

    public Canvas PopUpCanvas;
    public Button PopUpBackButton;
    public Button PopUpTryAgainButton;
    public Image FirstLetter;
    public Image LastLetter;
    public Image ImageResult;

    public Text AnswerText;
    public Text ResultText;
    public Text Counter;

    int whereToStart;
    char letter;

    // Use this for initialization
    void Start () {
        GameOver = false;
        Winner = false;
        PopUpCanvas.enabled = false;
        ResetAnswers();
        BuildString(letter);
        Counter.text = MenuScript.correctCount + " / 3";
    }
	
	// Update is called once per frame
	void Update () {
        if (!GameOver)
        {
            if (Winner == true && MenuScript.time > 1)
            {
                Counter.text = MenuScript.correctCount + " / 3";
                if (MenuScript.correctCount < 3)
                {
                    Winner = false;
                    ReloadPage();
                }
                else
                {
                    GameOver = true;
                    ResultText.text = "CONGRATULATIONS! \n YOU WIN!";
                    ImageResult.sprite = Resources.Load<Sprite>("fireman with cat");
                    PopUpCanvas.enabled = true;
                }
            }
            else if (MenuScript.time > 1) {
                MenuScript.time -= Time.deltaTime;
                string minutes = Mathf.Floor(MenuScript.time / 60).ToString("00");
                string seconds = Mathf.Floor(MenuScript.time % 60).ToString("00");
                Timer.text = "Time " + minutes + ":" + seconds;
            }
            else
            {
                GameOver = true;
                ResultText.text = "SORRY! \n YOU LOSE!";
                ImageResult.sprite = Resources.Load<Sprite>("fireman sad");
                PopUpCanvas.enabled = true;
            }
        }
	}

    public void GoToMenuScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    public void TryAgain()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void ReloadPage()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void BuildString(char rand)
    {
        clue = rand;
        if (rand.ToString() == "A")
        {
            if (whereToStart == 0)
            {
                answer = "EDCBA";
            }
            else
            {
                answer = "AGFED";
            }
        }
        else if (rand.ToString() == "B")
        {
            if (whereToStart == 0)
            {
                answer = "FEDCB";
            }
            else
            {
                answer = "BAGFE";
            }
        }
        else if (rand.ToString() == "C")
        {
            if (whereToStart == 0)
            {
                answer = "GFEDC";
            }
            else
            {
                answer = "CBAGF";
            }
        }
        else if (rand.ToString() == "D")
        {
            if (whereToStart == 0)
            {
                answer = "AGFED";
            }
            else
            {
                answer = "DCBAG";
            }
        }
        else if (rand.ToString() == "E")
        {
            if (whereToStart == 0)
            {
                answer = "BAGFE";
            }
            else
            {
                answer = "EDCBA";
            }
        }
        else if (rand.ToString() == "F")
        {
            if (whereToStart == 0)
            {
                answer = "CBAGF";
            }
            else
            {
                answer = "FEDCB";
            }
        }
        else if (rand.ToString() == "G")
        {
            if (whereToStart == 0)
            {
                answer = "DCBAG";
            }
            else
            {
                answer = "GFEDC";
            }
        }
        AnswerText.text = answer;
    }

    public void ResetAnswers()
    {
        letter = st[Random.Range(0, st.Length)];
        whereToStart = Random.Range(0, 2);
        if (whereToStart == 0)
        {
            Destroy(LastLetter.gameObject);
            FirstLetter.sprite = Resources.Load<Sprite>("letters/" + letter);
            FirstLetter.name = letter.ToString();
        }
        else
        {
            Destroy(FirstLetter.gameObject);
            LastLetter.sprite = Resources.Load<Sprite>("letters/" + letter);
            LastLetter.name = letter.ToString();
        }
    }
}
